package demo04_part1;

import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

public class MyProducer {

	public static void main(String[] args) {
		new MyProducer();
	}

	MyProducer() {
		KafkaProducer<Integer, String> producer = new KafkaProducer<>(configureKafkaProducer());
		String message = null;
		Integer key = 0;
		while (true) {
			message = "Message #" + key;
			Future<RecordMetadata> metadata = producer.send(new ProducerRecord<Integer, String>("test", key, message));
			try {
				System.out.println(message + " published on partition " + metadata.get().partition());
			} catch (Exception e) {
				System.out.println("error displaying metadata: " + e.getMessage());
			}
			key++;
			
			try { TimeUnit.SECONDS.sleep(1); } 
			catch (InterruptedException e) { e.printStackTrace(); }
		}
	}

	private Properties configureKafkaProducer() {
		Properties producerProperties = new Properties();

		producerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		producerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, 
				org.apache.kafka.common.serialization.IntegerSerializer.class);
		producerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, 
				org.apache.kafka.common.serialization.StringSerializer.class);
		//producerProperties.put(ProducerConfig.METADATA_MAX_AGE_CONFIG,20000);

		return producerProperties;
	}
}
