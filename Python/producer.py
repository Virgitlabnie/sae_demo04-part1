from time import sleep
from json import dumps
from kafka import KafkaProducer

producerProperties = { "bootstrap_servers":['localhost:9092'] } 
#producerProperties = { "bootstrap_servers":['localhost:9092'],
#                       "metadata_max_age_ms":20000 } 

producer = KafkaProducer(**producerProperties)

i = 0
while True:
   message = {"number":i}
   messageJson = dumps(message)
   messageBytes = messageJson.encode('utf-8')
   futureRecordMetadata = producer.send("test", value=messageBytes)
   print('{message} published on partition {partId}'.format(message=message, partId=futureRecordMetadata.get().partition))
   i+=1
   sleep(1)
