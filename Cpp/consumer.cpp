// Compile with :
// g++ -o consumer consumer.cpp `pkg-config --libs --cflags cppkafka`

// Basic consumer outputing what is produced on the topic "test"

#include <iostream>
#include <ostream>
#include <cppkafka/cppkafka.h>

int main() {
	// Create the config
	cppkafka::Configuration config = {
		{ "bootstrap.servers", "localhost:9092" },
		{ "auto.offset.reset", "earliest" },
//                { "metadata.max.age.ms", "60000" },
		{ "group.id", "myOwnPrivateCppGroup" }
	};

	// Create the consumer
	cppkafka::Consumer consumer(config);
	consumer.subscribe({"test"});

	// Read messages
	while(true) {
		auto msg = consumer.poll();
		if(msg && ! msg.get_error()) {
			auto message = std::string(msg.get_payload());
			auto partId = msg.get_partition();
			std::cout << message << " read from partition " << partId << std::endl;
		}
	}
}


